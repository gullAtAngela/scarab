// recommend command for personal widget
document.addEventListener("DOMContentLoaded", function(event)
	{var wLogic = "PERSONAL";
	ScarabQueue.push(['exclude', 'category', 'overlaps', ['Intimpflege']]);
	ScarabQueue.push(["recommend", {
		logic: wLogic,
		containerId: "widget-14",
		templateId: "widget-template",
		limit: 30,
		success: mySuccessHandler
	}]);

	if (wLogic == 'PERSONAL') {
		ScarabQueue.push(['go']);
	}
});


// recommend command for category widget
document.addEventListener("DOMContentLoaded", function(event) {
    var wLogic = "CATEGORY";                                                                                                                                                                                                                ScarabQueue.push(['exclude', 'category', 'overlaps', ['Intimpflege']]);
	ScarabQueue.push(["recommend", {
        logic: wLogic, // Name of the recommendatin widget
        containerId: "widget-6", // DOM element for rendering the widget
        templateId: "widget-template", // DOM Element with the template structur used to render
        limit: 30, // number of items to recommend
        success: mySuccessHandler // optional: cb function
    }]);
    
    if (wLogic == 'PERSONAL') {
        ScarabQueue.push(['go']);
    }
});


// cart command empty
ScarabQueue.push(['cart', []]);

// category command
ScarabQueue.push(["category", "Baby-Artikel"]);

// language command
ScarabQueue.push(['language', 'de']);

// exclude command
ScarabQueue.push(['exclude', 'category', 'overlaps', ['Intimpflege']]);

// go command
document.addEventListener("DOMContentLoaded", function(event) {
	ScarabQueue.push(["go"]);
});

// cart command with one item
ScarabQueue.push([
	'cart',
	[{	item: '24758',
		price: 17.95,
		quantity: 1
	}]
]);