# scarab
Demo of the Emarsys Scarab Integration

Predict is a recommendation engine which analyzes the behavior data collected from your web shop by our JavaScript API and uses this to deliver personalized recommendations to all your customers across email, mobile and web.

A self-learning marketing solution, Predict intelligently creates real-time, personalized content based on the online behavior of your entire customer base. It is flexible, measurable and reliable, and sits on top of an exceptionally lightweight client-side integration.

Anonymous behavior contributes to general affinity models covering your entire product catalog, while behavior from known customers (who have logged in or accessed the shop via a tracked link in an Emarsys campaign) is built into a unique, personal profile for that customer.

********************************************************************
Demo Link auf dem Extern-Angela Server
http://extern.angela.ch/ecommerce/scarab/

GitHub Project
https://github.com/gullAtAngela/scarab.git

Emarsys Help Portal
https://help.emarsys.com/hc/en-us/categories/115000604269

Predict Web Recommender - Overview
https://help.emarsys.com/hc/en-us/articles/115003845369-Predict-Web-Recommender-Overview